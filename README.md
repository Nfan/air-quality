```mermaid
graph TB
    localhost(localhost)
    subgraph airquality
        adminer(adminer) -- 5432 --- db
        db(db<br>Postgresql) -- 5432 --- web(web<br>Python Flask)
    end

    localhost -- 5000 --- web
```

# Requirements

* docker
* docker-compose

# Running

```shell
docker-compose up
```

# Access

[http://localhost:5000](http://localhost:5000)

Database can be checked using Adminer
[http://localhost:8080](http://localhost:8080)

* System: PostgreSQL
* Server: db
* Username: postgres
* Password: example

# Setup assumptions

Docker and docker compose are of one of the latest versions installed as
described in Docker docs

# Part 1

## Assumptions

* Dataset URL and filenames do not change
* As the task doesn't further require all the fields in data to be non null,
    missing values are not filled with values, e.g. mean in a column

## Solution

[app.py:init_airquality](web/app.py)

Database: postgresql 11

The data is downloaded and imported to the database if the table  with data
doesn't exists yet.

The csv file from the downloaded archive is imported into the pandas dataframe
and processed in the following way:

* All -200 values are replace with None. According to the dataset description
    these values are missing
* All empty column and rows are removed from the table.
* Time is formated: dots are replaced by ":".
* Column names are changed to properly import to the database. Everithing but 
    letters, figures and '\_' is replaced by '\_'
* Columns that contain only integer part are marked as integer, date is marked
    as date and time as time, to have proper formats in db and save on space

## Things to improve

Do the import of data in the Docker multi-stage build of the db image:

* Place processing script in /docker-entrypoint-initdb.d of the database
    container to be executed on db first start.
    As described here https://hub.docker.com/_/postgres in initialization
    scripts section
* Then copy initialized database data to the final container image

This will benefit in the following way:

* All processing dependencies won't be required in the running application
* Which will allow to use more lightweight image based on alpine

# Part 2

## Solution

Flask + SQLAlchemy + Marshmallow

The standard value is parsed from html page using BeautifulSoup and stored for
all subsequent reuests to the current worker.

The '/data' endpoint makes sql query to find all records with hour average NO2
greater than standard and joins with the table itself on 'date' to get the full
information during the days with high levels of NO2.

Using Marshmallow the requested data is returned to the client in JSON format.

A simple table is rendered on the client side using JS
[index.html](web/static/index.html)

## Things to improve

* Get number of days with high levels of NO2 (using group by)
* Aggregate data by date for easier consumption on the client side, eg.
    ```json
    ["01-01-1991": [{ "NO2": 1, "O2": 2, "Time": "00:00" }, {"NO2": 1.2 ... ] ]
    ```
* With the previous addition number of days can be easily counted on the client
    side
* Show separate tables for each day
* ~[Vega](https://vega.github.io/vega-lite/)~
* Separate config and the app
* Store Standard value in db / using werkzeug caching (memcache / file)
    to make it available across workers and add timeout for invalidation
* Add production server for flask, eg. gunicorn + nginx






# TODO

- [ ] Mutlti-stage Dockerfile for database. This way processing dependencies will be not required for running the app and more simple alpine-based image can be used.
  - [ ] Place the script that downloads and processes the Dataset in /docker-entrypoint-initdb.d.
  - [ ] Copy initialized db data to the db image
- [ ] Cache NO2 quality standard with fask-caching / in db / using simple proxy class
- [ ] Show total number of days matching the query requirements
- [ ] Format table

