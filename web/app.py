from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from bs4 import BeautifulSoup
import re
from urllib.request import urlopen
import pandas as pd
import numpy as np
import sqlalchemy.types
from sqlalchemy import create_engine
import requests
import zipfile
from io import BytesIO

STANDARDS_URL = "https://ec.europa.eu/environment/air/quality/standards.htm"
DATASET_URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/00360/AirQualityUCI.zip"
DB_NAME = 'air_quality'
NO2_STANDARD = None


def init_airquality(db_url):
  engine = create_engine(db_url)
  exists = engine.dialect.has_table(engine, DB_NAME)

  if not exists:
    print("Populating dataset data")
    req = requests.get(DATASET_URL)
    z = zipfile.ZipFile(BytesIO(req.content))
    csv = pd.read_csv(z.open("AirQualityUCI.csv"), delimiter=";", decimal=",", na_values="-200",
                      parse_dates=["Date"], dayfirst=True)

    csv.dropna(axis='columns', how='all', inplace=True)
    csv.dropna(how='all', inplace=True)
    csv['Time'] = csv['Time'].apply(lambda s: s.replace('.', ':'))
    # Replace special characters to later import to the sql table
    csv.columns = csv.columns.str.replace("[^a-zA-Z0-9_]", "_")

    int_cols = {}
    for col in csv:
        if col not in ["Date", "Time"]:
            idxs = csv[col].notnull()
            is_int = np.array_equal(csv[col][idxs], csv[col][idxs].astype(int))
            if is_int:
                int_cols[col] = sqlalchemy.types.INT

    time_cols = {"Date": sqlalchemy.types.DATE, "Time": sqlalchemy.types.TIME}
    col_types = {**int_cols, **time_cols}

    csv.to_sql(DB_NAME, engine, dtype=col_types)
    with engine.connect() as conn:
        conn.execute('ALTER TABLE air_quality ADD PRIMARY KEY (index);')


app = Flask(__name__, static_url_path='')
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:example@db:5432/postgres'
init_airquality(app.config['SQLALCHEMY_DATABASE_URI'])

db = SQLAlchemy(app)
ma = Marshmallow(app)

db.reflect()


def getNO2Standard():
  print(STANDARDS_URL)
  soup = BeautifulSoup(urlopen(STANDARDS_URL))
  main_content = soup.find('div', attrs={'class': "main-content"})
  rows = main_content.findAll('tr')
  for row in rows:
    if len(row.findAll(text=re.compile('.*NO2.*'))) > 0:
      tds = row.findAll('td')
      val = int(tds[1].p.text.split(" ")[0])
      return val


class AirQuality(db.Model):
    __tablename__ = 'air_quality'


class AirQualitySchema(ma.ModelSchema):
    class Meta:
        model = AirQuality


air_quality_schema = AirQualitySchema()


@app.route('/')
def index():
  return app.send_static_file('index.html')


@app.route('/data')
def hello():
  global NO2_STANDARD
  if NO2_STANDARD is None:
    print('update')
    NO2_STANDARD = getNO2Standard()
  dates = AirQuality.query.with_entities(AirQuality.Date).filter(
      AirQuality.NO2_GT_ > NO2_STANDARD).group_by(AirQuality.Date).subquery()
  r = AirQuality.query.join(dates, dates.c.Date == AirQuality.Date).order_by(AirQuality.Date, AirQuality.Time).all()
  return air_quality_schema.jsonify(r, many=True)
